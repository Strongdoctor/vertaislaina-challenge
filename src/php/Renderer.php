<?php
require "AutoInclude.php";

class Renderer {

	/**
	 * @return string
	 */
	public function generateUpperHtml() {
		return '
			<!DOCTYPE html>
			<html lang="en">
				<head>
					<link media="screen" rel="stylesheet" href="/src/css/stylesheets/screen.css" type="text/css">
					<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
				</head>
				<body>';
	}

	/**
	 * @return string
	 */
	public function generateLowerHtml() {
		return '<script src="/src/js/custom.js"></script>
				</body>
				</html>';
	}

	/**
	 * @return null
	 */
	public function renderFrontPage() {
		echo 	Renderer::generateUpperHtml()
				. Renderer::renderUserTable(Common::$users)
				. '<br>'
				. Renderer::renderAgeMoneyChart()
				. Renderer::generateLowerHtml();	
	}

	/**
	 * @return string
	 */
	private function renderUserTable() {

		$html = "";

		// The table headers
		$html .= '
		<h1>Users</h1>
		<table id="users_table">
		<thead>
			<tr>
				<th>ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>SSN</th>
				<th>Email</th>
				<th>Username</th>
				<th>Gender</th>
				<th>Age(years)</th>
				<th>Account(€)</th>
				<th>In debt?</th>
			</tr>
		</thead>
		<tbody>';

		// Build the table cells
		foreach (Common::$users as $user) {
			$indebted = Common::validateAccountBalance($user->getUserAccounts()[0]) ? "Nope" : "Yep";

			$html .= '<tr>';
			$html .= '<td class="id">' . $user->getID() . '</td>';
			$html .= '<td class="first_name">' . $user->getFirstName() . '</td>';
			$html .= '<td class="last_name">' . $user->getLastName() . '</td>';
			$html .= '<td class="ssn">' . $user->getSSN() . '</td>';
			$html .= '<td class="email">' . $user->getEmail() . '</td>';
			$html .= '<td class="username">' . $user->getUsername() . '</td>';
			$html .= '<td class="gender">' . Common::getGender($user) . '</td>';
			$html .= '<td class="age">' . Common::getAge($user) . '</td>';
			$html .= '<td class="money">' . $user->getUserAccounts()[0]->getAmount() . '</td>';
			$html .= '<td class="debt">' . $indebted . '</td>';
			$html .= '</tr>';
		}

		$html .= '</tbody>
			</table>';

		return $html;
	}

	/**
	 * @return string
	 */
	private function renderAgeMoneyChart() {
		$html = '
			<h1>Charts</h1>
			<div id="charts">
				<div class="chart_container">
					<h2>Age distribution</h2>
					<canvas id="age_chart"></canvas>
				</div>
				<div class="chart_container">
					<h2>Money(€) per age group</h2>
					<canvas id="age_money_chart"></canvas>
				</div>
				<div class="chart_container">
					<h2>Gender distribution</h2>
					<canvas id="gender_chart"></canvas>
				</div>
			</div>';
		return $html;
	}
}