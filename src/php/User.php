<?php

class User {

    private $data;
    private $accounts = [];

    /**
     * User constructor.
     *
     * @param array $data
     */
    public function __construct( &$data ) {
        $this->data =& $data;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->data['id'];
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->data['firstName'];
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->data['lastName'];
    }

    /**
     * @return string
     */
    public function getSSN() {
        return $this->data['ssn'];
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->data['email'];
    }

    /**
     * @return string
     */
    public function getUsername() {
        return $this->data['username'];
    }

    /**
     * @param UserAccount[] $userAccounts
     *
     * @return null
     */
    public function addUserAccount($userAccounts) {
        foreach ($userAccounts as $acc) {
            array_push($this->accounts, $acc);
        }
    }

    /**
     * @return UserAccount
     */
    public function getUserAccounts() {
        return $this->accounts;
    }
}