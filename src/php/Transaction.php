<?php

class Transaction {

    private $data;

    /**
     * Transaction constructor.
     *
     * @param array $data
     */
    public function __construct( &$data ) {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->data['id'];
    }

    /**
     * @return int
     */
    public function getUserId() {
        return $this->data['userId'];
    }

    /**
     * @return double
     */
    public function getAmount() {
        return $this->data['amount'];
    }

    /**
     * @return string
     */
    public function getAction() {
        return $this->data['action'];
    }

}