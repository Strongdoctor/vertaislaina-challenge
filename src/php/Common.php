<?php

class Common {

    public static $users = [];

    /**
     * @param bool $asArray
     *
     * @return User[]
     */
    private static function getSomeUsers( $asArray = false ) {
        $data = array(
            array(
                "id"    => 82910, "firstName" => "Jenna", "lastName" => "Viikate", "ssn" => "260633-298R",
                "email" => "Jenna.Viikate@example.net", "username" => "user18203" ), array(
                "id"    => 52407, "firstName" => "Atte", "lastName" => "Penttilä", "ssn" => "260136-305P",
                "email" => "Atte.Penttilä@example.net", "username" => "user81433" ), array(
                "id"    => 31783, "firstName" => "Tatu", "lastName" => "Vanhanen", "ssn" => "171176-292V",
                "email" => "Tatu.Vanhanen@example.com", "username" => "user15936" ), array(
                "id"    => 71822, "firstName" => "Tea", "lastName" => "Laakkonen", "ssn" => "191247-301S",
                "email" => "Tea.Laakkonen@example.com", "username" => "user48594" ), array(
                "id"    => 3363, "firstName" => "Inka", "lastName" => "Vanhanen", "ssn" => "021204A2181",
                "email" => "Inka.Vanhanen@example.org", "username" => "user13391" ), array(
                "id"    => 50258, "firstName" => "Sisu", "lastName" => "Ekman", "ssn" => "260195-7775",
                "email" => "Sisu.Ekman@example.net", "username" => "user89741" ), array(
                "id"    => 70467, "firstName" => "Juho", "lastName" => "Salmelainen", "ssn" => "210950-8471",
                "email" => "Juho.Salmelainen@example.com", "username" => "user11663" ), array(
                "id"    => 37415, "firstName" => "Topi", "lastName" => "Kantele", "ssn" => "050285-5395",
                "email" => "Topi.Kantele@example.org", "username" => "user65205" ), array(
                "id"    => 21386, "firstName" => "Joel", "lastName" => "Saanila", "ssn" => "140824-7285",
                "email" => "Joel.Saanila@example.net", "username" => "user67061" ), array(
                "id"    => 43296, "firstName" => "Tuukka", "lastName" => "Aarnio", "ssn" => "141041-215L",
                "email" => "Tuukka.Aarnio@example.org", "username" => "user49783" ), array(
                "id"    => 43174, "firstName" => "Nuutti", "lastName" => "Simo", "ssn" => "100928-3186",
                "email" => "Nuutti.Simo@example.org", "username" => "user2427" ), array(
                "id"    => 90392, "firstName" => "Riku", "lastName" => "Takkula", "ssn" => "250857-6774",
                "email" => "Riku.Takkula@example.com", "username" => "user30262" ), array(
                "id"    => 28810, "firstName" => "Aake", "lastName" => "Mujunen", "ssn" => "280861-855U",
                "email" => "Aake.Mujunen@example.org", "username" => "user40866" ), array(
                "id"    => 4590, "firstName" => "Pihla", "lastName" => "Mansikkaoja", "ssn" => "270226-1021",
                "email" => "Pihla.Mansikkaoja@example.net", "username" => "user1061" ), array(
                "id"    => 28515, "firstName" => "Karla", "lastName" => "Lappalainen", "ssn" => "071037-233M",
                "email" => "Karla.Lappalainen@example.com", "username" => "user64491" ), array(
                "id"    => 27889, "firstName" => "Valtteri", "lastName" => "Bräysy", "ssn" => "281134-5747",
                "email" => "Valtteri.Bräysy@example.org", "username" => "user34752" ), array(
                "id"    => 18777, "firstName" => "Jaakko", "lastName" => "Vesuri", "ssn" => "300746-0693",
                "email" => "Jaakko.Vesuri@example.org", "username" => "user86580" ), array(
                "id"    => 30824, "firstName" => "Amelia", "lastName" => "Juntti", "ssn" => "150300A210V",
                "email" => "Amelia.Juntti@example.net", "username" => "user22858" ), array(
                "id"    => 40855, "firstName" => "Riina", "lastName" => "Hanhela", "ssn" => "060882-198V",
                "email" => "Riina.Hanhela@example.org", "username" => "user62541" ), array(
                "id"    => 88949, "firstName" => "Rasmus", "lastName" => "Malmivaara", "ssn" => "230725-391J",
                "email" => "Rasmus.Malmivaara@example.net", "username" => "user51711" ), array(
                "id"    => 79359, "firstName" => "Lumina", "lastName" => "Arponen", "ssn" => "200940-356Y",
                "email" => "Lumina.Arponen@example.com", "username" => "user84011" ), array(
                "id"    => 95976, "firstName" => "Emma", "lastName" => "Kujala", "ssn" => "120917A443N",
                "email" => "Emma.Kujala@example.com", "username" => "user65151" ), array(
                "id"    => 28672, "firstName" => "Jaakko", "lastName" => "Aalto", "ssn" => "120310A6623",
                "email" => "Jaakko.Aalto@example.org", "username" => "user32855" ), array(
                "id"    => 96727, "firstName" => "Neea", "lastName" => "Vesuri", "ssn" => "291243-6094",
                "email" => "Neea.Vesuri@example.org", "username" => "user9889" ), array(
                "id"    => 5545, "firstName" => "Nella", "lastName" => "Timonen", "ssn" => "210131-103N",
                "email" => "Nella.Timonen@example.net", "username" => "user72870" ), array(
                "id"    => 4001, "firstName" => "Nuutti", "lastName" => "Närvälä", "ssn" => "230107A8883",
                "email" => "Nuutti.Närvälä@example.net", "username" => "user20007" ), array(
                "id"    => 62103, "firstName" => "Lumina", "lastName" => "Kynsilehto", "ssn" => "230467-350M",
                "email" => "Lumina.Kynsilehto@example.net", "username" => "user49639" ), array(
                "id"    => 43181, "firstName" => "Anu", "lastName" => "Väisälä", "ssn" => "271071-8474",
                "email" => "Anu.Väisälä@example.org", "username" => "user19415" ), array(
                "id"    => 46348, "firstName" => "Mika", "lastName" => "Parkkonen", "ssn" => "031271-262C",
                "email" => "Mika.Parkkonen@example.org", "username" => "user6519" ), array(
                "id"    => 15261, "firstName" => "Jenna", "lastName" => "Marjomaa", "ssn" => "311234-2683",
                "email" => "Jenna.Marjomaa@example.com", "username" => "user22833" ) );
        if ( $asArray )
            return $data;
        $result = array();
        foreach ( $data as &$userArray ) {
            $result[] = new User( $userArray );
        }
        return $result;
    }

    /**
     * @param bool $asArray
     *
     * @return UserAccount[]
     */
    private static function getSomeUserAccounts( $asArray = false ) {
        $data = array(
            array( "id" => 51109, "userId" => 82910, "iban" => "FI8295686549754530", "amount" => 2064.85 ),
            array( "id" => 21890, "userId" => 52407, "iban" => "FI5107323366987843", "amount" => 7962.98 ),
            array( "id" => 10685, "userId" => 31783, "iban" => "FI2903497873963014", "amount" => 2554.97 ),
            array( "id" => 97527, "userId" => 71822, "iban" => "FI9028434637125765", "amount" => 317.28 ),
            array( "id" => 45366, "userId" => 3363, "iban" => "FI2680073099676469", "amount" => 970.48 ),
            array( "id" => 26851, "userId" => 50258, "iban" => "FI7326518121308452", "amount" => 1553.31 ),
            array( "id" => 34874, "userId" => 70467, "iban" => "FI6926135998420087", "amount" => 6658.28 ),
            array( "id" => 99140, "userId" => 37415, "iban" => "FI9848084355558325", "amount" => 844.94 ),
            array( "id" => 38122, "userId" => 21386, "iban" => "FI9262766509699264", "amount" => 4764.36 ),
            array( "id" => 29524, "userId" => 43296, "iban" => "FI6361066043549590", "amount" => 7253.02 ),
            array( "id" => 23351, "userId" => 43174, "iban" => "FI4795954597095046", "amount" => 7179.19 ),
            array( "id" => 54153, "userId" => 90392, "iban" => "FI0235275020388654", "amount" => 8163.71 ),
            array( "id" => 67241, "userId" => 28810, "iban" => "FI3344671176435930", "amount" => 9346.21 ),
            array( "id" => 66363, "userId" => 4590, "iban" => "FI6225845915743490", "amount" => 7734 ),
            array( "id" => 76800, "userId" => 28515, "iban" => "FI6933994569531900", "amount" => 6487.9 ),
            array( "id" => 57662, "userId" => 27889, "iban" => "FI7480925011545029", "amount" => 5223.42 ),
            array( "id" => 27204, "userId" => 18777, "iban" => "FI5281728533956340", "amount" => 6074.24 ),
            array( "id" => 8325, "userId" => 30824, "iban" => "FI7535755707613595", "amount" => 2689.55 ),
            array( "id" => 93053, "userId" => 40855, "iban" => "FI4709834047404038", "amount" => 9807.04 ),
            array( "id" => 68402, "userId" => 88949, "iban" => "FI9528777519253581", "amount" => 7464.69 ),
            array( "id" => 55400, "userId" => 79359, "iban" => "FI0268035468430307", "amount" => 9148.89 ),
            array( "id" => 98388, "userId" => 95976, "iban" => "FI4947009843797630", "amount" => 8448.43 ),
            array( "id" => 90902, "userId" => 28672, "iban" => "FI6853316453490075", "amount" => 5470.15 ),
            array( "id" => 99168, "userId" => 96727, "iban" => "FI0333421624171718", "amount" => 6765.96 ),
            array( "id" => 21773, "userId" => 5545, "iban" => "FI0344905788947042", "amount" => 5695.57 ),
            array( "id" => 53656, "userId" => 4001, "iban" => "FI5525862706466314", "amount" => 6538.93 ),
            array( "id" => 96161, "userId" => 62103, "iban" => "FI1622073238286050", "amount" => 1632.13 ),
            array( "id" => 77674, "userId" => 43181, "iban" => "FI6723488930563755", "amount" => 3491.83 ),
            array( "id" => 32912, "userId" => 46348, "iban" => "FI1431213591586886", "amount" => 5606.04 ),
            array( "id" => 83697, "userId" => 15261, "iban" => "FI6151268777822879", "amount" => 5096.26 ) );
        if ( $asArray )
            return $data;
        $result = array();
        foreach ( $data as &$userAccountArray ) {
            $result[] = new UserAccount( $userAccountArray );
        }
        return $result;
    }

    /**
     * @param bool $asArray
     *
     * @return Transaction[]
     */
    private static function getSomeTransactions( $asArray = false ) {
        $data = array(
            array( "id" => 867616, "userId" => 82910, "amount" => 110.59, "action" => "deposit" ),
            array( "id" => 567688, "userId" => 52407, "amount" => 385.8, "action" => "withdraw" ),
            array( "id" => 152028, "userId" => 52407, "amount" => 486.96, "action" => "withdraw" ),
            array( "id" => 695591, "userId" => 52407, "amount" => 145.21, "action" => "withdraw" ),
            array( "id" => 991455, "userId" => 52407, "amount" => 315.3, "action" => "deposit" ),
            array( "id" => 533184, "userId" => 31783, "amount" => 263.73, "action" => "withdraw" ),
            array( "id" => 830732, "userId" => 31783, "amount" => 80.81, "action" => "deposit" ),
            array( "id" => 157854, "userId" => 71822, "amount" => 14.73, "action" => "withdraw" ),
            array( "id" => 729576, "userId" => 37415, "amount" => 383.27, "action" => "deposit" ),
            array( "id" => 342577, "userId" => 37415, "amount" => 154.91, "action" => "withdraw" ),
            array( "id" => 445379, "userId" => 37415, "amount" => 251.23, "action" => "deposit" ),
            array( "id" => 580084, "userId" => 37415, "amount" => 470.73, "action" => "withdraw" ),
            array( "id" => 111657, "userId" => 43174, "amount" => 383.85, "action" => "deposit" ),
            array( "id" => 620741, "userId" => 43174, "amount" => 446.95, "action" => "withdraw" ),
            array( "id" => 139201, "userId" => 43174, "amount" => 496.02, "action" => "withdraw" ),
            array( "id" => 261355, "userId" => 27889, "amount" => 140.49, "action" => "deposit" ),
            array( "id" => 666297, "userId" => 27889, "amount" => 358.4, "action" => "withdraw" ),
            array( "id" => 68258, "userId" => 79359, "amount" => 135.59, "action" => "deposit" ),
            array( "id" => 705542, "userId" => 79359, "amount" => 290.4, "action" => "deposit" ),
            array( "id" => 156240, "userId" => 79359, "amount" => 345.56, "action" => "deposit" ),
            array( "id" => 808035, "userId" => 5545, "amount" => 448.32, "action" => "withdraw" ),
            array( "id" => 418946, "userId" => 5545, "amount" => 364.99, "action" => "withdraw" ),
            array( "id" => 411927, "userId" => 5545, "amount" => 328.14, "action" => "deposit" ),
            array( "id" => 654860, "userId" => 5545, "amount" => 472.01, "action" => "withdraw" ),
            array( "id" => 184225, "userId" => 5545, "amount" => 96.92, "action" => "deposit" ) );
        if ( $asArray )
            return $data;
        $result = array();
        foreach ( $data as &$transaction ) {
            $result[] = new Transaction( $transaction );
        }
        return $result;
    }

    /**
     * @return null
     */
    public static function initializeUsers() {
        $users = Common::getSomeUsers();
        $userAccounts = Common::getSomeUserAccounts();
        $transactions = Common::getSomeTransactions();

        // We know the amount of Users and UserAccounts match
        for ($x = 0; $x < count($users); $x++) {
            $users[$x]->addUserAccount([$userAccounts[$x]]);
        }

        // Go through all the transactions
        // I won't be using validateAccountBalance here,
        // although I could, but
        // I will do so in the renderer to check who's indebted
        foreach ($transactions as $transaction) {
            foreach ($users as $user) {
                if ($transaction->getUserId() == $user->getId()) {

                    // In this case we know each user has only 1 account, this could be made much more generic,
                    // For example you could keep the total amount of money "per account", and add this onto that
                    // and then let them withdraw to whatever actual bank account, but there's no information
                    // as to what the requirements of the system is
                    $user->getUserAccounts()[0]->updateAmount(
                        $transaction->getAmount(),
                        $transaction->getAction()
                    );
                }
            }
        }

        Common::$users = $users;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public static function getGender( $user ) {

        // Get the "individual number"
        $indNumber = substr($user->getSSN(), -4, 3);

        //If indNumber is even, return F, else return M
        return ($indNumber % 2 == 0) ? 'F' : 'M';
    }

    /**
     * @param User $user
     *
     * @return int
     */
    public static function getAge( $user ) {
        
        // The unformatted birthday from the SSN
        $ssnBirthday = substr($user->getSSN(), 0, 6);
        $ssnYear = substr($user->getSSN(), 4, 2);
        $centurySign = substr($user->getSSN(), -5, 1);

        // We need the TZ to avoid weird scenarios
        $tz  = new DateTimeZone('Europe/Brussels');

        $correctedYear = "";
        // Check and correct according to century sign
        if ($centurySign == "+") {
            $correctedYear = "18" . $ssnYear;
        } else if ($centurySign == "-") {
            $correctedYear = "19" . $ssnYear;
        } else if ($centurySign == "A") {
            $correctedYear = "20" . $ssnYear;
        }
        
        $correctedBirthday = substr($ssnBirthday, 0, 4) . $correctedYear;

        // To DateTime object, 
        $birthday = DateTime::createFromFormat('dmY', $correctedBirthday, $tz);

        $age = $birthday->diff(new DateTime('now', $tz))->y;

        return $age;
    }

    /**
     * @param UserAccount $userAccount
     *
     * @return bool
     */
    public static function validateAccountBalance($userAccount) {
        // This function *SHOULD* be used at EVERY UserAccount-update
        // if you want to make sure someone isn't getting indebted
        // But for this assignment I've chosen to only use it
        // to render in the UI if someone is indebted

        $currentMoney = $userAccount->getAmount();

        if ($currentMoney >= 0) {
            return true;
        }
        return false;
    }

}