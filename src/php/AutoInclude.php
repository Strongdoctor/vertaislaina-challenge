<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

include_once "Transaction.php";
include_once "User.php";
include_once "UserAccount.php";
include_once "Common.php";
include_once "Renderer.php";
include_once "StupidRouter.php";
include_once "StupidApiController.php";