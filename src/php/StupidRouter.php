<?php
require "AutoInclude.php";

class StupidRouter {
	function __construct() {
		$this->renderer = new Renderer();
	}

	function route($request_uri, $method) {

		// Remove prepended and trailing forward slash
		$trimmed_uri = rtrim(ltrim($request_uri, '/'), '/');

		// Split request uri into individual terms, for example
		// /api/users becomes [api, users]
		$exploded_uri = explode("/", $trimmed_uri);

		if ($request_uri == '/') {
			$this->renderer->renderFrontPage();

		} else if ($exploded_uri[0] == 'api' && count($exploded_uri) > 1 && $method == 'GET') {
			echo StupidApiController::handle($exploded_uri[1]);
		} else {
			echo "404 my dude. <a href=\"/\">Go back</a>";
		}
	}
}