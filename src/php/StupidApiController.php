<?php
require "AutoInclude.php";

class StupidApiController {

	/**
     * @param string $request
     *
     * @return string
     */
	public static function handle($request) {
		if ($request == 'users') {
			$users_array = [];

			// Build associative array of users,
			// including account
			foreach (Common::$users as $user) {
				// We know the user has only 1 account
				$acc = $user->getUserAccounts()[0];
				array_push(
					$users_array,
					[
						"id"=>$user->getId(),
						"firstname"=>$user->getFirstName(),
						"lastname"=>$user->getLastName(),
						"ssn"=>$user->getSSN(),
						"email"=>$user->getEmail(),
						"username"=>$user->getUsername(),
						"account"=>[
							"id"=>$acc->getId(),
							"userid"=>$acc->getUserId(),
							"iban"=>$acc->getIBAN(),
							"amount"=>$acc->getAmount(),
						]
					]
				);
			}

			header('Content-Type: application/json');
			return json_encode($users_array);
		}
	}
}