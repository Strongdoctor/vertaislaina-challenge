<?php

class UserAccount {

    private $data;

    /**
     * UserAccount constructor.
     *
     * @param array $data
     */
    public function __construct( &$data ) {
        $this->data =& $data;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->data['id'];
    }

    /**
     * @return int
     */
    public function getUserId() {
        return $this->data['userId'];
    }

    /**
     * @return string
     */
    public function getIBAN() {
        return $this->data['iban'];
    }

    /**
     * @return double
     */
    public function getAmount() {
        return $this->data['amount'];
    }

    /**
     * @param float $amount | string $action
     *
     * @return float
     */
    public function updateAmount($amount, $action) {
        if ($action == "withdraw") {
            $this->data['amount'] = $this->data['amount'] - $amount;
        } else if ($action == "deposit") {
            $this->data['amount'] = $this->data['amount'] + $amount;
        }

        return $this->data['amount'];
    }

}