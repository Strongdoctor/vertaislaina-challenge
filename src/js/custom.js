window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(231,233,237)'
};

window.charts = [];

function generateAgeChart(rowList) {
	var ageList = [];

	for (var i = 0; i < rowList.length; i++) {
		const age = rowList[i].getElementsByClassName("age")[0].textContent;
		ageList.push(parseInt(age));
	}

	// Splitting into multiple arrays to make the code more clear
	var people_ages0_20 = [];
	var people_ages21_40 = [];
	var people_ages41_60 = [];
	var people_ages61_80 = [];
	var people_ages81p = [];

	for (var i = 0; i < ageList.length; i++) {
		const age = ageList[i];

		if (age <= 20) {
			people_ages0_20.push(age);
		} else if (age <= 40) {
			people_ages21_40.push(age);
		} else if (age <= 60) {
			people_ages41_60.push(age);
		} else if (age <= 80) {
			people_ages61_80.push(age);
		} else {
			people_ages81p.push(age);
		}
	}

	const datasets = [
		people_ages0_20.length,
		people_ages21_40.length,
		people_ages41_60.length,
		people_ages61_80.length,
		people_ages81p.length
	];

	const labels = ['0-20','21-40','41-60','61-80','81+'];
	const target = document.getElementById('age_chart');
	console.log("Datasets 2: " + datasets);
	generatePieChart('Age chart', datasets, labels, target);
}

function generateAgeMoneyChart(rowList) {
	// List of age-money relations
	var ageMoneyList = [];

	for (var i = 0; i < rowList.length; i++) {
		const age = rowList[i].getElementsByClassName("age")[0].textContent;
		const money = rowList[i].getElementsByClassName("money")[0].textContent;
		ageMoneyList.push(
			{
				age: parseInt(age),
				money: parseFloat(money)
			}
		);
	}

	// Pre-declare the lists containing people
	// of different ages
	var people_ages0_20 = [];
	var people_ages21_40 = [];
	var people_ages41_60 = [];
	var people_ages61_80 = [];
	var people_ages81p = [];

	// Put the data in the correct list
	for (var i = 0; i < ageMoneyList.length; i++) {
		const obj = ageMoneyList[i];

		if (obj.age <= 20) {
			people_ages0_20.push(obj);
		} else if (obj.age <= 40) {
			people_ages21_40.push(obj);
		} else if (obj.age <= 60) {
			people_ages41_60.push(obj);
		} else if (obj.age <= 80) {
			people_ages61_80.push(obj);
		} else {
			people_ages81p.push(obj);
		}
	}

	var money0_20 = 0;
	var money21_40 = 0;
	var money41_60 = 0;
	var money61_80 = 0;
	var money81p = 0;

	for (var i = 0; i < people_ages0_20.length; i++) {
		money0_20 += people_ages0_20[i].money;
	}

	for (var i = 0; i < people_ages21_40.length; i++) {
		money21_40 += people_ages21_40[i].money;
	}

	for (var i = 0; i < people_ages41_60.length; i++) {
		money41_60 += people_ages41_60[i].money;
	}

	for (var i = 0; i < people_ages61_80.length; i++) {
		money61_80 += people_ages61_80[i].money;
	}

	for (var i = 0; i < people_ages81p.length; i++) {
		money81p += people_ages81p[i].money;
	}

	const datasets = [money0_20, money21_40,money41_60,money61_80,money81p];
	const labels = ['0-20','21-40','41-60','61-80','81+'];
	console.log("Datasets 1: " + datasets);
	const target = document.getElementById('age_money_chart');
	console.log("Target: " + target);
	generatePieChart('Age-Money chart', datasets, labels, target);
}

function generateGenderChart(rowList) {
	var genderList = [];

	for (var i = 0; i < rowList.length; i++) {
		const gender = rowList[i].getElementsByClassName("gender")[0].textContent;
		genderList.push(gender);
	}

	// We presume there are only 2 genders
	var men = 0;
	var women = 0;

	for (var i = 0; i < genderList.length; i++) {
		const gender = genderList[i];
		if (gender == 'M') {men += 1} else {women += 1}
	}

	const datasets = [
		men,
		women
	];

	const labels = ['Men', 'Women'];
	const target = document.getElementById('gender_chart');
	generatePieChart('Gender chart', datasets, labels, target);
}


function generatePieChart(label, datasets, labels, target) {
	// Render the age-money chart
	const config = {
		type: 'pie',
		data: {
			datasets: [{
				data: datasets,
				backgroundColor: [
					window.chartColors.blue,
					window.chartColors.orange,
					window.chartColors.red,
					window.chartColors.green,
					window.chartColors.yellow,
					window.chartColors.purple,
				],
				label: label
			}],
			labels: labels
		},
		options: {
			responsive: true,
			maintainAspectRatio: false
		}
	};

	const ctx = target.getContext('2d');
	window.charts.push({ctx: ctx, config: config});
}

var table = document.getElementById("users_table");
var rowList = [];

for (var i = 0, row; row = table.rows[i]; i++) {
   rowList.push(row);
}	

// Remove first row (table headers) from the list
rowList.shift();

window.onload = function() {

	generateAgeChart(rowList);
	generateAgeMoneyChart(rowList);
	generateGenderChart(rowList);

	// Generating the charts without names because
	// We don't want to do anything with them
	// after rendering them
	for (var i = 0; i < window.charts.length; i++) {
		const chart_obj = window.charts[i];
		new Chart(chart_obj.ctx, chart_obj.config);
	}
};	