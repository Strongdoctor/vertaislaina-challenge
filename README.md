### How to run
Run: `php -S localhost:8000` while in the main folder (The one with index.php in it).

You can now access the site at `http://localhost:8000`.

With the server running, there's also an API that returns all users in JSON format at `GET: http://localhost:8000/api/users`.

### Third-party software
* `Compass` for SASS (CSS).
* `Chart.js` for the pie charts.

### WARNING!
For the front-end to render properly you need internet access (Chart.js is CDN-hosted)